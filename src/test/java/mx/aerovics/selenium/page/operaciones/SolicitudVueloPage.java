package mx.aerovics.selenium.page.operaciones;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.aerovics.selenium.page.BasePage;
import mx.aerovics.selenium.util.Constants;
import net.serenitybdd.core.pages.PageObject;

/**
 * @author scopeTI
 *
 */
public class SolicitudVueloPage extends PageObject{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudVueloPage.class);

	private BasePage basePage;

	By comboEmpresaSolicitante = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[5]/mat-form-field/div/div[1]/div[3]/input");
	By optionEmpresaSolicitante = By.xpath("/html/body/div[2]/div/div/div/mat-option[2]/span");
	//*[@id="mat-option-253"]/span
	
	By inputMotivoViaje = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[6]/mat-form-field/div/div[1]/div[3]/input");
	
	By inputFechaSalida = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[7]/mat-form-field/div/div[1]/div[3]/input");
	By inputHoraSalida = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[8]/mat-form-field/div/div[1]/div[4]/input");
	By inputFechaRegreso = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[9]/mat-form-field/div/div[1]/div[3]/input");
	By inputHoraRegreso = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[10]/mat-form-field/div/div[1]/div[4]/input");
	
	By inputRutaVuelo = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[13]/mat-form-field/div/div[1]/div[3]/input");
	By inputobservaciones = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[14]/mat-form-field/div/div[1]/div[3]/input");
	
	By alertValidate = By.xpath("//span[@class='mat-simple-snack-bar-content']");
	
	String xpathComboPasajero = "/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[11]/div/table/tbody/tr[::X::]/td/div/div/form/div[1]/div/mat-form-field/div/div[1]/div[3]/input";
	String xpathOptionPasajero = "/html/body/div[2]/div/div/div/mat-option[::X::]/span";
	
	String xpathButtonGuardarPasajero = "/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[11]/div/table/tbody/tr[::X::]/td/div/div/form/div[2]/div/button[1]";
	By iconAgregarPasajeroSolicitante = By.xpath("//*[@id=\"divPasajero_Solicitante\"]/div/div/button/span[1]/mat-icon");
	
	String xpathButtonCancelarPasajero = "/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/form/mdb-tabs/div/div[1]/div/div[11]/div/table/tbody/tr[::X::]/td/div/div/form/div[2]/div/button[2]";

	By buttonGuardarSolicitudVuelo = By.xpath("/html/body/app-root/app-solicitud_de_vuelo/section/div/div[2]/div/div/div/div/div/button[1]/span[1]");
	
    /**
     * @param driver
     */
    public SolicitudVueloPage(WebDriver driver) {
        super(driver);
    }

	/**
	 * @param empresaSolicitante
	 * @throws Exception
	 */
	public void seleccionaEmpresaSolicitante(String empresaSolicitante) throws Exception {
		try{
            basePage.click(comboEmpresaSolicitante);
            basePage.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            basePage.click(optionEmpresaSolicitante);
            basePage.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        }catch (Exception e){
            e.printStackTrace();
            Thread.sleep(10000);
        }
	}

	/**
	 * @param motivoViaje
	 * @throws Exception
	 */
	public void ingresaMotivoViaje(String motivoViaje) throws Exception{
		try{
			basePage.clear(inputMotivoViaje);
			basePage.sendKey(inputMotivoViaje,motivoViaje);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaFechaSalida(String fechaSalida) throws Exception{
		try{
			basePage.clear(inputFechaSalida);
			Thread.sleep(1000);
			basePage.find(inputFechaSalida).typeAndEnter(fechaSalida);
			basePage.sendKeyScape(inputFechaSalida);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaHoraSalida(String horaSalida) throws Exception{
		try{
			basePage.clear(inputHoraSalida);
			basePage.sendKey(inputHoraSalida,horaSalida);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaFechaRegreso(String fechaRegreso) throws Exception{
		try{
			basePage.clear(inputFechaRegreso);
			Thread.sleep(1000);
			basePage.find(inputFechaRegreso).typeAndEnter(fechaRegreso);
			basePage.sendKeyScape(inputFechaRegreso);
			//basePage.sendKeyTab(inputMotivoViaje);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaHoraRegreso(String horaRegreso) throws Exception{
		try{
			basePage.clear(inputHoraRegreso);
			basePage.sendKey(inputHoraRegreso,horaRegreso);
			//basePage.sendKeyTab(inputMotivoViaje);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void validarFechaRegresoMenorAFechaSalida() throws Exception{
		try{
			basePage.isPresent(alertValidate);
			Thread.sleep(3000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void validarHoraRegresoMenorAHoraSalida() throws Exception {
		try{
			basePage.isPresent(alertValidate);
			Thread.sleep(3000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickAgregarPasajeroSolicitante() throws Exception {
		try{
			LOGGER.info("icon agregar Pasajero");
			basePage.click(iconAgregarPasajeroSolicitante);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void seleccionaPasajeroSolicitante(String comboId, String optionId) throws Exception {
		try{
			String xPathGenericComboPasajero = xpathComboPasajero.replace(Constants.OPTION_X, comboId);
			By comboGenericPasajero = By.xpath(xPathGenericComboPasajero);
			
			String xPathGenericOptionPasajero = xpathOptionPasajero.replace(Constants.OPTION_X, optionId);
			By optionGenericPasajero = By.xpath(xPathGenericOptionPasajero);
			LOGGER.info("comboID Pasajero {} - {}", comboId, xPathGenericComboPasajero);
			basePage.click(comboGenericPasajero);
            Thread.sleep(1000);
            LOGGER.info("optionID Pasajero {} - {}", optionId, xPathGenericOptionPasajero);
            basePage.click(optionGenericPasajero);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void clickGuardarPasajeroSolicitante(String buttonId) throws Exception {
		try{
			String xPathGenericButtonGuardarPasajero = xpathButtonGuardarPasajero.replace(Constants.OPTION_X, buttonId);
			By buttonGenericGuardarPasajero = By.xpath(xPathGenericButtonGuardarPasajero);
			
			LOGGER.info("buttonGuardarPasajeroID {}", buttonGenericGuardarPasajero);
			basePage.click(buttonGenericGuardarPasajero);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void clickCancelarAgregarSolicitante(String buttonId) throws Exception{
		try{
			String xPathGenericButtonCancelarPasajero = xpathButtonCancelarPasajero.replace(Constants.OPTION_X, buttonId);
			By ButtonGenericCancelarPasajero = By.xpath(xPathGenericButtonCancelarPasajero);
			
			LOGGER.info("buttonCancelarPasajeroID {}", ButtonGenericCancelarPasajero);
			basePage.click(ButtonGenericCancelarPasajero);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void validarPermitirPasajeroRepetido() throws Exception {
		try{
			basePage.isPresent(alertValidate);
			Thread.sleep(3000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaRutaVuelo(String rutaVuelo) throws Exception {
		try{
			basePage.sendKey(inputRutaVuelo,rutaVuelo);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaObservaciones(String observaciones) throws Exception {
		try{
			basePage.sendKey(inputobservaciones,observaciones);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void guardarSolicitudVuelo() throws Exception {
		try{
			basePage.click(buttonGuardarSolicitudVuelo);
			Thread.sleep(5000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

}
