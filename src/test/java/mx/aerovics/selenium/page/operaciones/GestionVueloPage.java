package mx.aerovics.selenium.page.operaciones;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.aerovics.selenium.page.BasePage;
import mx.aerovics.selenium.util.Constants;
import net.serenitybdd.core.pages.PageObject;

/**
 * @author scopeTI
 *
 */
public class GestionVueloPage extends PageObject{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GestionVueloPage.class);

	private BasePage basePage;
	By buttonPestaniaRegistroVuelo = By.xpath("/html/body/app-root/app-dynamicsearch/section/div/div[3]/div/div/div[1]/button[1]");
	
	String xpathFilaSolicitud = "/html/body/app-root/app-dynamicsearch/section/div/div[2]/div[1]/div/div/div/table/tbody/tr[2]/td[1]";
	By labelNumeroSolicitud = By.xpath("/html/body/app-root/app-dynamicsearch/section/div/div[2]/div[1]/div/div/div/table/tbody/tr[2]/td[1]");
	By buttonNuevoRegistroVuelo = By.xpath("/html[1]/body[1]/app-root[1]/app-dynamicsearch[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[2]/button[1]/span[1]");
	
	By divComboMatricula = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[3]/mat-form-field/div/div[1]/div[4]");
	By inputComboMatricula = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[3]/mat-form-field/div/div[1]/div[3]/input");
	By optionOneComboMatricula = By.xpath("/html/body/div[3]/div[3]/div/div/mat-option[1]");
	
	By divComboOrigen = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[9]/mat-form-field/div/div[1]/div[4]");
	By inputComboOrigen = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[9]/mat-form-field/div/div[1]/div[3]/input");
	By optionOneComboOrigen = By.xpath("/html/body/div[3]/div[3]/div/div/mat-option[1]");
	
	By divComboTipoVuelo = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[7]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]");
	By optionSelectComboTipoVueloComercial = By.xpath("/html/body/div[3]/div[4]/div/div/div/mat-option[2]");

	By divComboDestiono = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[10]/mat-form-field/div/div[1]/div[4]");
	By inputComboDestiono = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[1]/div/div/div[10]/mat-form-field/div/div[1]/div[3]/input");
	By optionOneComboDestino = By.xpath("/html/body/div[3]/div[3]/div/div/mat-option[1]");
			
	//Pestania Tripulantes
	By linkPestaniaTripulacion = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/mat-tab-header/div/div/div/div[2]");
	By iconAgregaTripulante = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[2]/div/div/div/div/div/button/span[1]/mat-icon");

	String xpathComboTripulante = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[2]/div/div/div/div/table/tbody/tr[::X::]/td/div/div/form/div[1]/div[2]/mat-form-field/div/div[1]/div[4]";
	By inputComboTripulante = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[2]/div/div/div/div/table/tbody/tr[2]/td/div/div/form/div[1]/div[2]/mat-form-field/div/div[1]/div[3]/input");
	By optionOneComboTripulante = By.xpath("/html/body/div[3]/div[3]/div/div/mat-option[1]");
	
	String xpathComboCargo = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[2]/div/div/div/div/table/tbody/tr[::X::]/td/div/div/form/div[1]/div[1]/mat-form-field/div/div[1]/div[3]/mat-select/div/div[2]";
	String xpathOptionComboCargo = "/html/body/div[3]/div[4]/div/div/div/mat-option[::X::]";
	
	String xpathButtonGuardarTripulante = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[2]/div/div/div/div/table/tbody/tr[::X::]/td/div/div/form/div[2]/div/button[1]";
	String xpathButtonCancelarTripulante = "/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body[2]/div/div/div/div/table/tbody/tr[::X::]/td/div/div/form/div[2]/div/button[2]/span[1]";
	
	//Pestania Pasajeros
	By buttonPestaniaPasajero = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[1]/div/div/div/form/mat-tab-group/mat-tab-header/div/div/div/div[3]");
	
	
	By buttonGuardarTramo = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[2]/button[1]");
	By buttonGuardarVuelo = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-registro-de-vuelo/div[2]/button[2]");
	
	By alertValidate = By.xpath("//span[@class='mat-simple-snack-bar-content']");
	
	/**
	 * Icono de edicion de la tabla gestion de vuleo
	 * Corresponde a la fila 1
	 */
	By iconoEditar = By.xpath("/html/body/app-root/app-dynamicsearch/section/div/div[3]/div/div/div[2]/table/tbody/tr/td[1]/div/a");
	
	By inputComisariato = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-comisariato-y-notas-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body/div/div/div[3]/mat-form-field/div/div[1]/div[3]/textarea");
	By inputNotasVueloComisariato = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-comisariato-y-notas-de-vuelo/div[1]/div/div/div/form/mat-tab-group/div/mat-tab-body/div/div/div[4]/mat-form-field/div/div[1]/div[3]/textarea");
	By buttonGuardarComisariato = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-comisariato-y-notas-de-vuelo/div[2]/div/button[1]");
	By buttonCancelarComisariato = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-modal-comisariato-y-notas-de-vuelo/div[2]/div/button[3]");
	
	By pestaniaComisariato = By.xpath("/html/body/app-root/app-dynamicsearch/section/div/div[3]/div/div/div[1]/button[2]");

	public void clickNumeroSolicitud() throws Exception{
		try{
			String numeroSolicitud = basePage.find(labelNumeroSolicitud).getTextValue();
			LOGGER.info("folio de solicitud {}", numeroSolicitud);
			//TODO Mejorar mecanismo de click en componente
			basePage.click(labelNumeroSolicitud);
			Thread.sleep(5000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void clickNumeroSolicitudByFila(String numeroFila) throws Exception{
		try{
			String numeroSolicitud = basePage.find(labelNumeroSolicitud).getTextValue();
			LOGGER.info("folio de solicitud {}", numeroSolicitud);
			By labelNumeroSolicitudBy = By.xpath(xpathFilaSolicitud.replace(Constants.OPTION_X, numeroFila));
			basePage.click(labelNumeroSolicitudBy);
			Thread.sleep(5000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickPestaniaRegistroVuelo() throws Exception{
		try{
			basePage.click(buttonPestaniaRegistroVuelo);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickNuevoRegistroVuelo() throws Exception{
		try{
			basePage.click(buttonNuevoRegistroVuelo);
			basePage.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void seleccionaMatricula(String matricula) throws Exception{
		try{
            basePage.click(divComboMatricula);
            Thread.sleep(2000);
            basePage.sendKey(inputComboMatricula, matricula);
            Thread.sleep(2000);
            basePage.click(optionOneComboMatricula);
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
            Thread.sleep(10000);
        }
	}

	public void seleccionaOrigen(String origen) throws Exception{
		try{
			basePage.click(divComboOrigen);
            Thread.sleep(1000);
            basePage.sendKey(inputComboOrigen, origen);
            Thread.sleep(1000);
            basePage.click(optionOneComboOrigen);
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
            Thread.sleep(10000);
        }
	}

	public void seleccionaTipoVuelo(String tipoVuelo) throws Exception{
		LOGGER.info("seleccionaTipoVuelo {}", tipoVuelo);
		Thread.sleep(1000);
        basePage.click(divComboTipoVuelo);
        Thread.sleep(1000);
        
        if(tipoVuelo.equals(Constants.TIPO_VUELO_COMERCIAL)){
        	basePage.click(optionSelectComboTipoVueloComercial);
        }else {
        	basePage.click(optionSelectComboTipoVueloComercial);
        }
	}

	public void seleccionaDestino(String destino) throws Exception{
		try{
			basePage.click(divComboDestiono);
            Thread.sleep(1000);
            basePage.sendKey(inputComboDestiono, destino);
            Thread.sleep(1000);
            basePage.click(optionOneComboDestino);
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
            Thread.sleep(10000);
        }
	}

	public void clickPestaniaTripulacion() throws Exception{
		try{
			basePage.click(linkPestaniaTripulacion);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickAgregarTripulante() throws Exception{
		try{
			LOGGER.info("clickAgregarTripulante");
			basePage.click(iconAgregaTripulante);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void seleccionaCargo(String comboId, String optionId) throws Exception{
		try{
			String xPathGenericComboCargo = xpathComboCargo.replace(Constants.OPTION_X, comboId);
			By comboGenericCargo = By.xpath(xPathGenericComboCargo);
			
			String xPathGenericOptionCargo = xpathOptionComboCargo.replace(Constants.OPTION_X, optionId);
			By optionGenericCargo = By.xpath(xPathGenericOptionCargo);
			
			LOGGER.info("comboID Tripulante cargo {} - {}", comboId, xPathGenericComboCargo);
			basePage.click(comboGenericCargo);
            Thread.sleep(1000);
            LOGGER.info("optionID Tripulante cargo {} - {}", optionId, xPathGenericOptionCargo);
            basePage.click(optionGenericCargo);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void seleccionaTripulante(String comboId, String nombre) throws Exception{
		try{
			String xPathGenericComboTripulante = xpathComboTripulante.replace(Constants.OPTION_X, comboId);
			By ComboGenericTripulante = By.xpath(xPathGenericComboTripulante);
			
			LOGGER.info("comboID Tripulante {} - {}", comboId, xPathGenericComboTripulante);
			basePage.click(ComboGenericTripulante);
            LOGGER.info("nombre Tripulante {}", nombre);
            Thread.sleep(3000);
            basePage.sendKey(inputComboTripulante, nombre);
            Thread.sleep(2000);
            basePage.click(optionOneComboTripulante);
            Thread.sleep(2000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickGuardarTripulante(String buttonId) throws Exception{
		try{
			String xPathGenericButtonGuardar = xpathButtonGuardarTripulante.replace(Constants.OPTION_X, buttonId);
			By buttonGenericGuardar = By.xpath(xPathGenericButtonGuardar);
			
			LOGGER.info("buttonGuardarPasajeroID {}", buttonGenericGuardar);
			basePage.click(buttonGenericGuardar);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void clickPestaniaPasajeros() throws Exception{
		try{
			basePage.click(buttonPestaniaPasajero);
			Thread.sleep(4000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void clickGuardarTramo() throws Exception{
		try{
			basePage.click(buttonGuardarTramo);
			Thread.sleep(5000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickGuardarVuelo() throws Exception{
		try{
			basePage.click(buttonGuardarVuelo);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	public void validaGuardadoTramo() throws Exception{
		try{
			basePage.click(buttonGuardarVuelo);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	
	//FIN REGISTRO VUELO
	
	//INICIA COMISARIATO

	public void clickPestaniaComisariato() throws Exception{
		try{
			basePage.click(pestaniaComisariato);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void clickIconoEditar() throws Exception{
		try{
			basePage.click(iconoEditar);
			Thread.sleep(2000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaComisariato(String comisariato) throws Exception{
		try{
			basePage.clear(inputComisariato);
			basePage.sendKey(inputComisariato,comisariato);
            Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void ingresaNotasVuelo(String notasVuelo) throws Exception{
		try{
			basePage.clear(inputNotasVueloComisariato);
			basePage.sendKey(inputNotasVueloComisariato,notasVuelo);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}

	public void guardarComisariato() throws Exception{
		try{
			//basePage.click(buttonGuardarComisariato);
			basePage.click(buttonCancelarComisariato);
			Thread.sleep(1000);
		}catch (Exception e) {
			e.printStackTrace();
			Thread.sleep(10000);
		}
	}
	//FIN COMISARIATO
}
