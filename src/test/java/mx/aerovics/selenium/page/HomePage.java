package mx.aerovics.selenium.page;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.serenitybdd.core.pages.PageObject;

public class HomePage extends PageObject{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class);

	private BasePage basePage;
	
	By imgLogo = By.xpath("//img[@alt='Portal Logo']");
	By textPrincipal =  By.xpath("//*[@id=\"xe4df2d46db4deb08fc3c71198c96199b\"]");
	
	By buttonIniciarSesion = By.xpath("/html/body/app-root/app-signin/div/div/div/form/div[7]/button");
	By inputUsuario = By.id("mat-input-0");
	By inputContrasenia = By.id("mat-input-1");
	By checkCondiciones = By.xpath("/html/body/app-root/app-signin/div/div/div/form/div[4]/div/div/label/input");
	By buttonNuevaSolicitudVuelo = By.xpath("//*[@id=\"btnAddVuelo\"]");
	
	private By linkOperaciones = By.linkText("Operaciones");
	private By linkOperacionesSolicitudVuelo = By.linkText("Solicitud de Vuelo");
	private By linkOperacionesSolicitudVueloGestionVuelo = By.linkText("Gestión de Vuelo");
		
	private By logo = By.xpath("/html/body/app-root/app-sidebar/div/aside/div/ul/li[1]/img");
	
    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void esperaCargaPagina() throws Exception {
        Thread.sleep(5000);
        assertTrue("FAIL : TEXT Principal no visible", isElementVisible(textPrincipal));
    }

	public void autenticaUsuario(String usuario, String contrasenia) throws Exception {
		try {
			basePage.sendKey(inputUsuario, usuario);
			basePage.sendKey(inputContrasenia, contrasenia);
			basePage.find(checkCondiciones).click();
			basePage.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			basePage.click(buttonIniciarSesion);
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
		}
	}
	
	public void clickNuevaSolicitudVuelo() throws Exception{
		try {
			basePage.click(buttonNuevaSolicitudVuelo);
			Thread.sleep(2000);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickMenuOperarciones() throws Exception{
		try {
			basePage.click(linkOperaciones);
			Thread.sleep(2000);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickMenuOperacionesSolicitudVuelo() throws Exception{
		try {
			basePage.click(linkOperacionesSolicitudVuelo);
			
			Thread.sleep(2000);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickMenuOperarcionesSolicitudVueloGestionVuelo() throws Exception{
		try {
			basePage.click(logo);
			basePage.sendKeys(Keys.PAGE_DOWN);
			Thread.sleep(2000); 
			basePage.click(linkOperacionesSolicitudVueloGestionVuelo);
			Thread.sleep(3000);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
