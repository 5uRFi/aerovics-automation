package mx.aerovics.selenium.page;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class BasePage extends PageObject {

    @Managed
    private static WebDriver driver;

    //Constructor inicializado
    public BasePage(WebDriver driver) {
        BasePage.driver = driver;
    }

    public By selectGeneric = By.xpath("//*[@id=\"select2-drop\"]");

    //Accion click
    public void click(By element) throws Exception {
        try {
            driver.findElement(element).click();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("No se pudo dar click sobre el elemento: " + element);
        }
    }

    public void clear(By element) throws Exception {
        try {
            driver.findElement(element).clear();
        } catch (Exception e) {
            throw new Exception("No se pudo dar click sobre el elemento: " + element);
        }
    }


    //Accion de devolver un booleano si el elemento esta Visible
    public boolean isDisplayed(By element) throws Exception {
        try {
            return driver.findElement(element).isDisplayed();
        } catch (Exception e) {
            throw new Exception("No se pudo devolver el valor del elemento: " + element);
        }
    }

    //Accion de devolver un boolean si el elemento esta Habilitado
    public boolean isEnabled(By element) throws Exception {
        try {
            return driver.findElement(element).isEnabled();
        } catch (Exception e) {
            throw new Exception("No se pudo devolver el valor del elemento: " + element);
        }
    }

    //Accion obtener texto
    public String getText(By element) throws Exception {
        try {
            return driver.findElement(element).getText();
        } catch (Exception e) {
            throw new Exception("No se puedo obtener el texto del elemento: " + element);
        }
    }

    //Accion obtener texto
    public String getTextByAttribute(By element) throws Exception {
        try {
            return driver.findElement(element).getAttribute("value");
        } catch (Exception e) {
            throw new Exception("No se puedo obtener el texto del elemento: " + element);
        }
    }

    //Accion escribir caja de texto
    public void sendKey(By element, String textoCaja) throws Exception {
        try {
            driver.findElement(element).sendKeys(textoCaja);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("No se escribir dentro del elemento: " + element);
        }
    }


    //Accion Tab
    public void sendKeyTab(By element) throws Exception {
        try {
            driver.findElement(element).sendKeys(Keys.TAB);
        } catch (Exception e) {
            throw new Exception("No se escribir dentro del elemento: " + element);
        }
    }


    //Accion Escape
    public void sendKeyScape(By element) throws Exception {
        try {
            driver.findElement(element).sendKeys(Keys.ESCAPE);
        } catch (Exception e) {
            throw new Exception("No Realiza el Teclado Escape: " + element);
        }
    }
    //Accion obtiene el titulo de la pagina
    public String getTitle() {
          return driver.getTitle();
    }

    //Accion de seleccion Lista - Combo Box
    public void selectValue(By element, String valor) throws Exception {
        try {
            //return driver.findElement(element).getText();
            Select se = new Select(driver.findElement(element));
            se.selectByVisibleText(valor);
        } catch (Exception e) {
            throw new Exception("No se pudo seleccionar el elemento" + element);
        }
    }

    public void selectValueFromLi(By element, String valor) throws Exception {
        WebElement dropdown = driver.findElement(element);
        List<WebElement> options = dropdown.findElements(By.tagName("li"));
        for (WebElement option : options)
        {
            if (option.getText().toUpperCase().contains(valor.toUpperCase())){
                option.click();
                break;
            }
        }
    }
    public void uploadFile(By element, String dirFile) throws Exception {
        try{
            WebElement uploadFile = driver.findElement(element);
            uploadFile.sendKeys(dirFile);
        }catch (Exception e){
            throw new Exception("No se pudo cargar el archivo: " + dirFile);
        }
    }

    public WebElement selectValueGetFirst(By element) throws Exception {
        try {
            //return driver.findElement(element).getText();
            Select se = new Select(driver.findElement(element));
            return se.getFirstSelectedOption();
        } catch (Exception e) {
            throw new Exception("No se pudo seleccionar el elemento" + element);
        }
    }

    //Esperar elemento sea Visible
    public void waitVisible(By element) throws Exception
    {
        try
        {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        }
        catch (Exception e)
        {
            throw new Exception("No se pudo esperar el elemento " + e);
        }
    }

    public void waitIsNotVisible(By element) throws Exception {
        try{
            WebDriverWait wait = new WebDriverWait(driver,60);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("No se pudo esperar el elemento");
        }
    }

    //ESPERAR QUE EL ELEMENTO SEA CLICLEABLE
    public void waitClicleable(By element) throws Exception
    {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
        catch (Exception e)
        {
            throw new Exception("No se pudo esperar el elemento " + e);
        }
    }

    //ESPERA QUE LA ALERTA ESTE PRESENTE
    public void waitPresent() throws Exception
    {
        try
        {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.alertIsPresent());
        }
        catch (Exception e)
        {
            throw new Exception("No se pudo esperar el elemento " + e);
        }
    }

    public boolean equals(By element, String textObjeto) throws Exception {
        try {
          return  driver.findElement(element).equals(textObjeto);
        } catch (Exception e) {
            throw new Exception("El Texto es diferente al Elemento: " + element);
        }
    }

    public boolean isPresent(By element) throws Exception {
        try {
            return  driver.findElement(element).isDisplayed();
        } catch (Exception e) {
            throw new Exception("El Texto es diferente al Elemento: " + element);
        }
    }
    
    /**
     * Presiona una tecla
     * @param key
     * @throws Exception 
     */
    public void sendKeys(Keys key) throws Exception {
    	try {
        	withAction().sendKeys(key).perform();
        } catch (Exception e) {
            throw new Exception("Eror al presionar la tecla", e);
        }
    }
    

}
