package mx.aerovics.selenium.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {
                "html:target/build/cucumber-html-report",
                "pretty:target/build/cucumber-pretty.txt"
        },
        features = {"src/test/resources/features/operaciones/operaciones-gestionvuelo-comisariato.feature"},
        glue= {"mx.aerovics.selenium.stepdefinition"},
        snippets = CucumberOptions.SnippetType.CAMELCASE)

public class ComisariatoRunnerTest {
}