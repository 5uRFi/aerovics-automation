package mx.aerovics.selenium.stepdefinition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Y;
import mx.aerovics.selenium.page.HomePage;
import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.steps.ScenarioSteps;

public class HomeStep extends ScenarioSteps{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6646862419021867971L;

	private static final Logger LOGGER = LoggerFactory.getLogger(HomeStep.class);

	private HomePage homePage;
	
	@Screenshots
	@Cuando("el usuario entra a la url de la aplicacion")
	public void elUsuarioEntraALaUrlDeLaAplicacion() throws Exception {
		LOGGER.info("el usuario entra a la url de la aplicacion");
		homePage.getDriver().manage().window().maximize();
        homePage.open();
	}

	@Screenshots
	@Y("el usuario autentica con usuario y contrasenia {string} {string}")
	public void elUsuarioAutenticaConUsuarioYContrasenia(String usuario, String contrasenia) throws Exception {
		LOGGER.info("el usuario autentica con usuario y contrasenia {}", usuario);
	    homePage.autenticaUsuario(usuario, contrasenia);
	}
	
	@Screenshots
	@Y("el usuario da click en el boton nueva solicitud de vuelo")
	public void elUsuarioDaClickEnElBotonNuevaSolicitudDeVuelo() throws Exception{
		LOGGER.info("el usuario da click en el boton nueva solicitud de vuelo");
		homePage.clickNuevaSolicitudVuelo();
	}

	@Screenshots
	@Y("el usuario da click en el menu operacion")
	public void elUsuarioDaClickEnElMenuOperacion() throws Exception{
		LOGGER.info("el usuario da click en el menu operacion");
		homePage.clickMenuOperarciones();
	}

	@Screenshots
	@Y("el usuario da click en el menu solicitud de vuelo")
	public void elUsuarioDaClickEnElMenuSolicitudDeVuelo() throws Exception{
		LOGGER.info("el usuario da click en el menu solicitud de vuelo");
		homePage.clickMenuOperacionesSolicitudVuelo();
	}

	@Screenshots
	@Y("el usuario da click en el menu gestion de vuelo")
	public void elUsuarioDaClickEnElMenuGestionDeVulo() throws Exception{
		LOGGER.info("el usuario da click en el menu gestion de vuelo");
		homePage.clickMenuOperarcionesSolicitudVueloGestionVuelo();
	}
}
