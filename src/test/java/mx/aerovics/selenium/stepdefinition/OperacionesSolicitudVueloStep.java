package mx.aerovics.selenium.stepdefinition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.cucumber.java.es.Y;
import mx.aerovics.selenium.page.operaciones.SolicitudVueloPage;
import net.thucydides.core.annotations.Screenshots;

public class OperacionesSolicitudVueloStep {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OperacionesSolicitudVueloStep.class);
	
	private SolicitudVueloPage solicitudVueloPage;
	
	private Integer trPasajero = 1;

	@Screenshots
	@Y("el usuario selecciona empresa solicitante {string}")
	public void elUsuarioSeleccionaEmpresaSolicitante(String empresaSolicitante) throws Exception {
		LOGGER.info("el usuario selecciona empresa solicitante {}", empresaSolicitante);
		solicitudVueloPage.seleccionaEmpresaSolicitante(empresaSolicitante);
	}

	@Screenshots
	@Y("el usuario ingresa motivo del viaje {string}")
	public void elUsuarioIngresaMotivoDelViaje(String motivoViaje) throws Exception {
		LOGGER.info("el usuario ingresa motivo del viaje {}", motivoViaje);
	    solicitudVueloPage.ingresaMotivoViaje(motivoViaje);
	}

	@Screenshots
	@Y("el usuario ingresa fecha de salida {string}")
	public void elUsuarioIngresaFechaDeSalida(String fechaSalida) throws Exception {
		LOGGER.info("el usuario ingresa fecha de salida {}", fechaSalida);
		solicitudVueloPage.ingresaFechaSalida(fechaSalida);
	}

	@Screenshots
	@Y("el usuario ingresa hora de salida {string}")
	public void elUsuarioIngresaHoraDeSalida(String horaSalida) throws Exception {
		LOGGER.info("el usuario ingresa hora de salida {}", horaSalida);
		solicitudVueloPage.ingresaHoraSalida(horaSalida);
	}

	@Screenshots
	@Y("el usuario ingresa fecha de regreso {string}")
	public void elUsuarioIngresaFechaDeRegreso(String fechaRegreso) throws Exception {
		LOGGER.info("el usuario ingresa fecha de regreso {}", fechaRegreso);
		solicitudVueloPage.ingresaFechaRegreso(fechaRegreso);
	}

	@Screenshots
	@Y("validar que la fecha de regreso no sea menor a la fecha de salida")
	public void validarQueLaFechaDeRegresoNoSeaMenorALaFechaDeSalida() throws Exception {
		LOGGER.info("validar que la fecha de regreso no sea menor a la fecha de salida");
		solicitudVueloPage.validarFechaRegresoMenorAFechaSalida();
	}

	@Screenshots
	@Y("el usuario ingresa hora de regreso {string}")
	public void elUsuarioIngresaHoraDeRegreso(String horaRegreso) throws Exception {
		LOGGER.info("el usuario ingresa hora de regreso {}", horaRegreso);
		solicitudVueloPage.ingresaHoraRegreso(horaRegreso);
	}

	@Screenshots
	@Y("validar que la hora de regreso no sea menor a la hora de salida")
	public void validarQueLaHoraDeRegresoNoSeaMenorALaHoraDeSalida() throws Exception {
		LOGGER.info("validar que la hora de regreso no sea menor a la hora de salida");
	    solicitudVueloPage.validarHoraRegresoMenorAHoraSalida();
	}
	
	//@Screenshots
	@Y("el usuario agrega y guarda N pasajeros solicitantes {string}")
	public void elUsuarioAgregaYGuardaNPasajerosSolicitantes(String totalPasajeros) throws Exception {
		Integer intTotalPasajeros = Integer.valueOf(totalPasajeros) + 1;
		//trPasajero = 1;
		LOGGER.info("el usuario agrega y guarda N pasajeros solicitantes {}", intTotalPasajeros);
	    for (int i = 1; i < Integer.valueOf(intTotalPasajeros); i++) {
	    	if(trPasajero == 6) {
	    		trPasajero = 1;
	    	}
	    	agregaUsuario(trPasajero, i);
			trPasajero ++;
		}
	}
	
	@Screenshots
	private void agregaUsuario(Integer trPasajero,  Integer i) throws Exception{
		LOGGER.info("pasajeroSolicitante {}", i);
		solicitudVueloPage.clickAgregarPasajeroSolicitante();
		Thread.sleep(1000);
		solicitudVueloPage.seleccionaPasajeroSolicitante(String.valueOf(trPasajero*2), String.valueOf(i));
		Thread.sleep(1000);
		solicitudVueloPage.clickGuardarPasajeroSolicitante(String.valueOf(trPasajero*2));
		Thread.sleep(1000);
	}
	
	@Screenshots
	@Y("el usuario da click en agregar pasajero solicitante")
	public void elUsuarioDaClickEnAgregarPasajeroSolicitante() throws Exception {
		LOGGER.info("el usuario da click en agregar pasajero solicitante");
	    solicitudVueloPage.clickAgregarPasajeroSolicitante();
	}
	
	@Screenshots
	@Y("el usuario selecciona pasajero solicitante repetido")
	public void elUsuarioSeleccionaPasajeroSolicitanteRepetido() throws Exception {
		int trInicialComboPasajero = 1;
		if(trPasajero == 6) {
    		trPasajero = 1;
    	}
		LOGGER.info("el usuario selecciona pasajero solicitante repetido {}", trPasajero);
		solicitudVueloPage.seleccionaPasajeroSolicitante(String.valueOf(trPasajero*2), String.valueOf(trInicialComboPasajero));
	}
	
	/*
	@Screenshots
	@Y("el usuario selecciona pasajero solicitante")
	public void elUsuarioSeleccionaPasajeroSolicitante() throws Exception {
		int trComboPasajero = 4;
		int idOptionPasajero = 17;
		LOGGER.info("el usuario agrega pasajero solicitante trCombo-idOption {} - {}", trComboPasajero, idOptionPasajero);
		solicitudVueloPage.seleccionaPasajeroSolicitante(String.valueOf(trComboPasajero), String.valueOf(idOptionPasajero));
	}
	*/
	
	/*
	@Screenshots
	@Y("el usuario guarda pasajero solicitante")
	public void elUsuarioGuardaPasajeroSolicitante() throws Exception {
		LOGGER.info("el usuario agrega pasajero solicitant {}", trPasajero);
	    //solicitudVueloPage.clickGuardarPasajeroSolicitante(String.valueOf(trInicialButtonGuardarPasajero));
	    solicitudVueloPage.clickGuardarPasajeroSolicitante(String.valueOf(trPasajero*2));
	    trPasajero ++;
	}*/
	
	@Screenshots
	@Y("el usuario cancela agregar pasajero solicitante")
	public void elUsuarioCancelaAgregarSolicitanteRepetido() throws Exception {
		LOGGER.info("el usuario cancela agregar pasajero solicitante {}", trPasajero);
		solicitudVueloPage.clickCancelarAgregarSolicitante(String.valueOf(trPasajero*2));
	}

	@Screenshots
	@Y("validar que no se permita agregar pasajero repetido")
	public void validarQueNoSePermitaAgregarPasajeroRepetido() throws Exception {
		LOGGER.info("validar que no se permita agregar pasajero repetido");
	    solicitudVueloPage.validarPermitirPasajeroRepetido();
	}

	@Screenshots
	@Y("el usuario ingresa ruta de vuelo {string}")
	public void elUsuarioIngresaRutaDeVuelo(String rutaVuelo) throws Exception {
		LOGGER.info("el usuario ingresa ruta de vuelo {}", rutaVuelo);
	    solicitudVueloPage.ingresaRutaVuelo(rutaVuelo);
	}

	@Screenshots
	@Y("el usuario ingresa observaciones {string}")
	public void elUsuarioIngresaObservaciones(String observaciones) throws Exception {
		LOGGER.info("el usuario ingresa observaciones {}", observaciones);
		solicitudVueloPage.ingresaObservaciones(observaciones);
	}

	@Screenshots
	@Y("el usuario guarda la solicitud")
	public void elUsuarioGuardaLaSolicitud() throws Exception {
		LOGGER.info("el usuario guarda la solicitud");
		solicitudVueloPage.guardarSolicitudVuelo();
	}
}
