package mx.aerovics.selenium.stepdefinition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.cucumber.java.es.Y;
import mx.aerovics.selenium.page.operaciones.GestionVueloPage;
import mx.aerovics.selenium.util.Constants;
import net.thucydides.core.annotations.Screenshots;

public class OperacionesGestionVuelo {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OperacionesGestionVuelo.class);
	
	private GestionVueloPage gestionVueloPage;

	@Screenshots
	@Y("el usuario da click en el numero de solicitud")
	public void elUsuarioDaClickEnElNumeroDeSolicitud() throws Exception{
		//TODO Revisar opcion de enviar file o extraer id
		LOGGER.info("el usuario da click en el numero de solicitud");
		gestionVueloPage.clickNumeroSolicitud();
	}
	
	@Screenshots
	@Y("el usuario da click en el numero de solicitud de fila1")
	public void elUsuarioDaClickEnElNumeroDeSolicitudDeFila1() throws Exception{
		//TODO Revisar opcion de enviar file o extraer id
		LOGGER.info("el usuario da click en el numero de solicitud de fila1");
		gestionVueloPage.clickNumeroSolicitudByFila("1");
	}

	@Screenshots
	@Y("el usuario da click en la pestania registro de vuelo")
	public void elUsuarioDaClickEnLaPestaniaRegistroDeVuelo() throws Exception{
		LOGGER.info("el usuario da click en la pestania registro de vuelo");
		gestionVueloPage.clickPestaniaRegistroVuelo();
	}

	@Screenshots
	@Y("el usuario da click en nuevo registro de vuelo")
	public void elUsuarioDaClickEnNuevoRegistroDeVuelo() throws Exception{
		LOGGER.info("el usuario da click en nuevo registro de vuelo");
	    gestionVueloPage.clickNuevoRegistroVuelo();
	}

	@Screenshots
	@Y("el usuario selecciona matricula {string}")
	public void elUsuarioSeleccionaMatricula(String matricula) throws Exception{
		LOGGER.info("el usuario selecciona matricula {}", matricula);
		gestionVueloPage.seleccionaMatricula(matricula);
	}

	@Screenshots
	@Y("el usuario selecciona origen {string}")
	public void elUsuarioSeleccionaOrigen(String origen) throws Exception{
		LOGGER.info("el usuario selecciona origen {}", origen);
		gestionVueloPage.seleccionaOrigen(origen);
	}

	@Screenshots
	@Y("el usuario selecciona tipo de vuelo {string}")
	public void elUsuarioSeleccionaTipoDeVuelo(String tipoVuelo) throws Exception{
		LOGGER.info("el usuario selecciona tipo de vuelo {}", tipoVuelo);
	    gestionVueloPage.seleccionaTipoVuelo(tipoVuelo);
	}

	@Screenshots
	@Y("el usuario selecciona destino {string}")
	public void elUsuarioSeleccionaDestino(String destino) throws Exception{
		LOGGER.info("el usuario selecciona tipo de vuelo {}", destino);
		gestionVueloPage.seleccionaDestino(destino);
	}
	
	@Screenshots
	@Y("el usuario selecciona pestania tripulacion")
	public void elUsuarioSeleccionaPestaniaTripulacion() throws Exception{
		LOGGER.info("el usuario selecciona pestania tripulacion");
		gestionVueloPage.clickPestaniaTripulacion();
	}

	@Screenshots
	@Y("el usuario agrega a un tripulante {string} {string} {string}")
	public void elUsuarioAgregaAUnComandateALaTripulacion(String numero, String cargo, String nombre) throws Exception{
		LOGGER.info("tripulanteSolicitante {} - {} - {}", numero,cargo,nombre);
		
		String optionIdCargo = getCargoSeleccionado(cargo);
		String trTripulante = String.valueOf(Integer.valueOf(numero) * 2);
		
		gestionVueloPage.clickAgregarTripulante();
		Thread.sleep(1000);
		gestionVueloPage.seleccionaCargo(trTripulante, optionIdCargo);
		Thread.sleep(1000);
		gestionVueloPage.seleccionaTripulante(trTripulante, nombre);
		Thread.sleep(1000);
		gestionVueloPage.clickGuardarTripulante(trTripulante);
		Thread.sleep(1000);
	}
	
	/**
	 * Retorna idCargo
	 * @param cargo
	 * @return
	 */
	private String getCargoSeleccionado(String cargo) {
		String optionIdCargo = "2";
		if(cargo.equals(Constants.TRIPULANTE_COMANDANTE)) {
			optionIdCargo = "2";
		}else if(cargo.equals(Constants.TRIPULANTE_PRIMER_OFICIAL)) {
			optionIdCargo = "3";
		}else if(cargo.equals(Constants.TRIPULANTE_SOBRECARGO)) {
			optionIdCargo = "4";
		}else if(cargo.equals(Constants.TRIPULANTE_CAPITAN)) {
			optionIdCargo = "5";
		}else if(cargo.equals(Constants.TRIPULANTE_SEGUNDO_TRIPULANTE)) {
			optionIdCargo = "6";
		}
		return optionIdCargo;
	}

	@Screenshots
	@Y("el usuario da click en la pestania pasajeros")
	public void elUsuarioDaClickEnLaPestaniaPasajeros() throws Exception{
		LOGGER.info("el usuario da click en la pestania registro de vuelo");
		gestionVueloPage.clickPestaniaPasajeros();
	}
	
	@Screenshots
	@Y("el usuario da click en guardar tramo")
	public void elUsuarioDaClickEnGuardarTramo() throws Exception{
		LOGGER.info("el usuario da click en guardar tramo");
		gestionVueloPage.clickGuardarTramo();
	}
	
	@Screenshots
	@Y("el usuario da click en el numero de solicitud a guardar")
	public void elUsuarioDaClickEnElNumeroDeSolicitudAGuardar() throws Exception{
	    gestionVueloPage.clickNumeroSolicitudByFila("1");
	}

	@Screenshots
	@Y("el usuario da click en guardar vuelo")
	public void elUsuarioDaClickEnGuardarVuelo() throws Exception{
	    gestionVueloPage.clickGuardarVuelo();
	}
	
	//FIN REGISTRO VUELO
	
	//INICIA COMISARIATO
	@Screenshots
	@Y("el usuario da click en el numero de solicitud de fila2")
	public void elUsuarioDaClickEnElNumeroDeSolicitudDeFila2() throws Exception{
	    LOGGER.info("el usuario da click en el numero de solicitud de fila2");
		gestionVueloPage.clickNumeroSolicitudByFila("2");;
	}
	
	@Screenshots
	@Y("el usuario da click en la pestania comisariato")
	public void elUsuarioDaClickEnLaPestaniaComisariato() throws Exception{
	    LOGGER.info("el usuario da click en la pestania comisariato");
		gestionVueloPage.clickPestaniaComisariato();
	}

	@Screenshots
	@Y("el usuario da click en el icono de accion editar gestion de vuelo")
	public void elUsuarioDaClickEnElIconoDeAccionEditarGestionDeVuelo() throws Exception{
		LOGGER.info("el usuario da click en el icono de accion editar registro de vuelo");
	    gestionVueloPage.clickIconoEditar();
	}
	
	@Screenshots
	@Y("el usuario da click en el icono de accion editar registro de vuelo")
	public void elUsuarioDaClickEnElIconoDeAccionEditarRegistroDeVuelo() throws Exception{
		LOGGER.info("el usuario da click en el icono de accion editar registro de vuelo");
	    gestionVueloPage.clickIconoEditar();
	}

	@Screenshots
	@Y("el usuario ingresa comisariato {string}")
	public void elUsuarioIngresaComisariato(String comisariato) throws Exception{
	    LOGGER.info("el usuario ingresa comisariato {}", comisariato);
	    gestionVueloPage.ingresaComisariato(comisariato);
	}

	@Screenshots
	@Y("el usuario ingresa notas de vuelo {string}")
	public void elUsuarioIngresaNotasDeVuelo(String notasVuelo) throws Exception{
		LOGGER.info("el usuario ingresa notas de vuelo {}", notasVuelo);
	    gestionVueloPage.ingresaNotasVuelo(notasVuelo);
	}

	@Screenshots
	@Y("el usuario da click en guardar comisariato")
	public void elUsuarioDaClickEnGuardarComisariato() throws Exception{
		LOGGER.info("el usuario da click en guardar comisariato");
		gestionVueloPage.guardarComisariato();
	}

}
