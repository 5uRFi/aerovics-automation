package mx.aerovics.selenium.util;

public class Constants {
    public static final String SI = new String ("SI");
    public static final String OPTION_X = new String ("::X::");
    
    public static final String TIPO_VUELO_COMERCIAL = new String ("COMERCIAL");
    
    public static final String TRIPULANTE_COMANDANTE = new String ("COMANDANTE");
    public static final String TRIPULANTE_PRIMER_OFICIAL = new String ("PRIMER OFICIAL");
    public static final String TRIPULANTE_SOBRECARGO = new String ("SOBRECARGO");
    public static final String TRIPULANTE_CAPITAN = new String ("CAPITAN");
    public static final String TRIPULANTE_SEGUNDO_TRIPULANTE = new String ("SEGUNDO TRIPULANTE");
}
