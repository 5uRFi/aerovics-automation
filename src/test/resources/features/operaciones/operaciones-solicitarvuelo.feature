#language: es
  @aerovics-automation

  Característica: Operaciones - solicitud de vuelo - solicitar vuelo

  @OperacionesSolicitudVuelo
  Esquema del escenario: Crear solicitud de vuelo
    Cuando el usuario entra a la url de la aplicacion
    Y el usuario autentica con usuario y contrasenia "<usuario>" "<contrasenia>" 
    Y el usuario da click en el boton nueva solicitud de vuelo
    Y el usuario selecciona empresa solicitante "<empresaSolicitante>"
    Y el usuario ingresa motivo del viaje "<motivoViaje>"
    Y el usuario ingresa fecha de salida "<fechaSalida>"
    Y el usuario ingresa fecha de regreso "<fechaRegresoIncorrecta>"
    Y el usuario ingresa hora de salida "<horaSalida>"
    Y validar que la fecha de regreso no sea menor a la fecha de salida
    Y el usuario ingresa fecha de regreso "<fechaRegresoCorrecta>"
    Y el usuario ingresa hora de regreso "<horaRegresoIncorrecta>"
    Y validar que la hora de regreso no sea menor a la hora de salida
    Y el usuario ingresa hora de regreso "<horaRegresoCorrecta>"
    #add 16 pajeros
    Y el usuario agrega y guarda N pasajeros solicitantes "<totalPasajeros>"
    Y el usuario da click en agregar pasajero solicitante
    Y el usuario selecciona pasajero solicitante repetido
    Y validar que no se permita agregar pasajero repetido
    Y el usuario cancela agregar pasajero solicitante
   	Y el usuario ingresa ruta de vuelo "<rutaVuelo>"
   	Y el usuario ingresa observaciones "<observaciones>"
   	Y el usuario guarda la solicitud

    Ejemplos:
    | usuario | contrasenia | empresaSolicitante                | motivoViaje      | fechaSalida | horaSalida | fechaRegresoIncorrecta | fechaRegresoCorrecta | horaRegresoIncorrecta | horaRegresoCorrecta | totalPasajeros | rutaVuelo     | observaciones     |
    | QASV 	  | Prueba1234  | EL PALACIO DE HIERRO S.A. DE C.V. | PRUEBAS ATM      | 23/10/2023  | 12:00   	  | 20/10/2023             | 23/10/2023           | 11:00                 | 14:00               | 2              | textRutaVuelo | textObservaciones |