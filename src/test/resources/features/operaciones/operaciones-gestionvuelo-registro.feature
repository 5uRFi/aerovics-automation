#language: es
  @aerovics-automation

  Característica: Operaciones - solicitud de vuelo - gestion vuelo - registro vuelo

  @OperacionesGestionVueloRegistroVuelo
  Esquema del escenario: gestion de solicitud de vuelo
    Cuando el usuario entra a la url de la aplicacion
    Y el usuario autentica con usuario y contrasenia "<usuario>" "<contrasenia>" 
    Y el usuario da click en el menu operacion
    Y el usuario da click en el menu solicitud de vuelo
    Y el usuario da click en el menu gestion de vuelo
    Y el usuario da click en el numero de solicitud de fila2
    Y el usuario da click en la pestania registro de vuelo
    Y el usuario da click en nuevo registro de vuelo
    Y el usuario selecciona matricula "<matricula>"
    Y el usuario selecciona origen "<origen>"
    Y el usuario selecciona tipo de vuelo "<tipoVuelo>"
    Y el usuario selecciona destino "<destino>"
    Y el usuario selecciona pestania tripulacion
    Y el usuario agrega a un tripulante "<numeroTripulante>" "<cargoTripulante>" "<nombreTripulante>"
    Y el usuario da click en la pestania pasajeros
    Y el usuario da click en guardar tramo
    
    #TODO Solo acepta 1 tripulante - atributo numeroTripulante
    Ejemplos:
    | usuario | contrasenia | matricula | origen                       | tipoVuelo | destino                      | numeroTripulante | cargoTripulante | nombreTripulante        |
    | ejuarez | Prueba2023  | 450       | EDICIÓN (AAAA) (EDIC) (EDIC) | COMERCIAL | ADDISON - ADDISON MUNI (2A8) | 1                | COMANDANTE      | jorge castillo castillo |
    
    
  @OperacionesGestionVueloRegistroVueloGuardarVuelo
  Esquema del escenario: gestion de solicitud de vuelo
    Cuando el usuario entra a la url de la aplicacion
    Y el usuario autentica con usuario y contrasenia "<usuario>" "<contrasenia>" 
    Y el usuario da click en el menu operacion
    Y el usuario da click en el menu solicitud de vuelo
    Y el usuario da click en el menu gestion de vuelo
    Y el usuario da click en el numero de solicitud a guardar
    Y el usuario da click en el icono de accion editar registro de vuelo
    Y el usuario da click en guardar vuelo
    
    Ejemplos:
    | usuario | contrasenia |
    | ejuarez | Prueba2023  |