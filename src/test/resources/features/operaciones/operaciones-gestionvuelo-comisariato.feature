#language: es
  @aerovics-automation

  Característica: Operaciones - solicitud de vuelo - gestion vuelo - comisariato

  @OperacionesGestionVueloComisariato
  Esquema del escenario: gestion de solicitud de vuelo
    Cuando el usuario entra a la url de la aplicacion
    Y el usuario autentica con usuario y contrasenia "<usuario>" "<contrasenia>" 
    Y el usuario da click en el menu operacion
    Y el usuario da click en el menu solicitud de vuelo
    Y el usuario da click en el menu gestion de vuelo
    Y el usuario da click en el numero de solicitud de fila2
    Y el usuario da click en la pestania comisariato
    Y el usuario da click en el icono de accion editar gestion de vuelo 
    Y el usuario ingresa comisariato "<comisariato>"
    Y el usuario ingresa notas de vuelo "<notasVuelo>"
    Y el usuario da click en guardar comisariato
    
    Ejemplos:
    | usuario | contrasenia | comisariato     | notasVuelo     |
    | ejuarez | Prueba2023  | textComisariato | textNotasVuelo |